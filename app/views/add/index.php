<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="<?= BASEURL;?>/css/style.css">
<style>
form{
  background-color: #f2f2f2;
  border-radius: 8px;
  padding: 5px;
  width :50%;
}
</style>


</head>
<body>
<div class = "top-container"> 
    <div class="tittle">Product Add</div>
    <input onclick ="validateInput();" type="submit" value="Save" form = "product_form">
    <button onclick = "window.location='<?= BASEURL;?>';" class = "button" id = "cancel-button">Cancel</button>
</div>

<hr>

   <form id="product_form"  action="<?= BASEURL;?>/add-product/add" method='post'>

    <div class="container">
    <label for="sku">SKU</label>
    <input type="text" id="sku" name="sku" placeholder="SKU" required='' oninvalid="this.setCustomValidity('Please, submit required data')">
    </div>

    <div class="container">
    <label for="name">Name</label>
    <input type="text" id="name" name="name" placeholder="Name" required='' oninvalid="this.setCustomValidity('Please, submit required data')">
    </div>

    <div class="container">
    <label for="price">Price</label>
    <input type="text" id="price" name="price" placeholder="price" >
    </div>

    <div class="container">
    <label for="type">Type</label>
    <select id="productType" name="type" onchange="show();">
      <option value="dvd">DVD</option>
      <option value="book">Book</option>
      <option value="furniture">Furniture</option>
    </select>
    </div>

    <!-- <div class="container">
    <label for="subject">Subject</label>
    <textarea id="subject" name="subject" placeholder="Write something.." style="height:200px"></textarea>
    </div> -->
    
    <div class="specification"  id="dvd">
        <div class="container">
            <label for="size">Size</label>
            <input type="text" id="size" name="size" placeholder="size" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');">
        </div>
    </div>

    <div class="specification"  id="furniture">
        <div class="container">
            <label for="height">Height</label>
            <input type="text" id="height" name="height" placeholder="height" >
        </div>
        <div class="container">
            <label for="width">Width</label>
            <input type="text" id="width" name="width" placeholder="width" >
        </div>
            <div class="container">
            <label for="length">Length</label>
            <input type="text" id="length" name="length" placeholder="length" >
        </div>
    </div>

    <div class="specification" id="book">
        <div class="container">
            <label for="weight">Weight</label>
            <input type="text" id="weight" name="weight" placeholder="weight" >
        </div>
    </div>

  </form>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script src="<?= BASEURL;?>/js/script.js"></script>



</body>
</html>
