<?php

class ProductModel extends Dbh{
    private $product = [];
    public function getProducts(){
        $sql = "SELECT product.*, furniture.spec as Spec FROM `product`
        RIGHT JOIN furniture ON furniture.SKU = product.SKU";
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute();
        $this->product = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $sql = "SELECT product.*, book.spec as Spec FROM `product`
        RIGHT JOIN book ON book.sku = product.SKU";
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute();
        $this->product = array_merge($this->product,$stmt->fetchAll(PDO::FETCH_ASSOC));
        $sql = "SELECT product.*, dvd.spec as Spec FROM `product`
        RIGHT JOIN dvd ON dvd.sku = product.SKU";
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute();
        $this->product = array_merge($this->product,$stmt->fetchAll(PDO::FETCH_ASSOC));
        return $this->product;
    }
    
    public function deleteProduct($sku){
        $sql = "DELETE FROM product WHERE sku = '$sku'";
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute();
    }

    protected function addProductTable($sku, $price, $name, $type){
        $sql = "INSERT INTO `product`(`SKU`, `Name`, `Price`, `Type`) VALUES ('$sku', '$name', $price, '$type') ";
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute();
    }

    public function getProductBySku($sku){
        $sql = "SELECT * FROM `product` WHERE SKU = '$sku'";
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute();
        $this->product = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $this->product;
    }
}


