<?php
class Home extends Controller{
    public function index(){
        $data= $this->model('ProductModel')->getProducts();
        $this->view('home/index', $data);
    }

    public function delete(){
        foreach($_POST['sku'] as $sku){
            $this->model('ProductModel')->deleteProduct($sku);
        }
        header("Location: " . BASEURL . "/home/index");
        exit;
    }

}