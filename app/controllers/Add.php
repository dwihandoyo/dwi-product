<?php
abstract class Product {
    public $sku;
    public $name;
    public $price;
    public $type;
    abstract public function addProduct($model);
    public function validateSku($model, $sku){
        return $model->getProductBySku($sku);
    }
}

class Book extends Product{
  public $weight;
  public $spec;
    public function setAttribute($data){
        $this->sku = $data["sku"];
        $this->name = $data["name"];
        $this->price = $data["price"];
        $this->type = $data["type"];
        $this->weight = $data["weight"];
        $this->id = $this->generateId();
        $this->spec = "$this->weight kg";
    }

    public function addProduct($model){
        $model->addProduct($this->sku, $this->price, $this->name, $this->type, $this->id, $this->weight, $this->spec);
    }

    public function generateId(){
        $id =  substr(uniqid("BK"),0,10);
        return $id;
    }
}

class Dvd extends Product{
  public $size;
  public $spec;
    public function setAttribute($data){
        $this->sku = $data["sku"];
        $this->name = $data["name"];
        $this->price = $data["price"];
        $this->type = $data["type"];
        $this->size = $data["size"];
        $this->id = $this->generateId();
        $this->spec = "$this->size MB";
    }
    public function addProduct($model){
        $model->addProduct($this->sku, $this->name, $this->price, $this->type, $this->id, $this->size, $this->spec);
    }

    public function generateId(){
        $id =  substr(uniqid("DV"),0,10);
        return $id;
    }
    
}

class Furniture extends Product{
  public $height;
  public $width;
  public $length;
  public $spec;
    public function setAttribute($data){
        $this->sku = $data["sku"];
        $this->name = $data["name"];
        $this->price = $data["price"];
        $this->type = $data["type"];
        $this->height = $data["height"];
        $this->width = $data["width"];
        $this->length = $data["length"];
        $this->id = $this->generateId();
        $this->spec = "$this->height"."x"."$this->width"."x"."$this->length";
    }
    public function addProduct($model){
        $model->addProduct($this->sku, $this->name, $this->price, $this->type, $this->id, $this->height, $this->width, $this->length, $this->spec);
    }

    public function generateId(){
        $id =  substr(uniqid("FR"),0,10);
        return $id;
    }
    
}

class Add extends Controller{
    public function index(){
        $this->view('add/index');
    }
    public function add(){
        if(isset($_POST['type'])){
            
            $type = ucfirst($_POST['type']);
            $product = new $type;
            $product->setAttribute($_POST);
            $model = $this->model($type."Model");
            if(count($product->validateSku($model, $_POST['sku']))==0){
                $product->addProduct($model);
                $this->redirect(BASEURL.'/');
            }
            else{
                $this->redirect(BASEURL.'/add-product');
                // header("Location: " . BASEURL . "/add-product");
                // exit;
            }
        }
        
    }
    
    public function redirect($url){
        echo '<script type="text/javascript">';
        echo 'window.location.href="'.$url.'";';
        echo '</script>';
        echo '<noscript>';
        echo '<meta http-equiv="refresh" content="0;url='.$url.'" />';
        echo '</noscript>'; exit;
    }
}