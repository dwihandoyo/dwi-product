
let dvd = document.getElementById("dvd");
let book = document.getElementById("book");
let furniture = document.getElementById("furniture");
let size = document.getElementById("size");
let height = document.getElementById("height");
let width = document.getElementById("width");
let furLength = document.getElementById("length");
let weight = document.getElementById("weight");
let type =  document.getElementById("productType");

dvd.style.display = "block";
book.style.display = "none";
furniture.style.display = "none";
size.required = true;
height.required = false;        
width.required = false;
furLength.required = false;
weight.required = false;
function show() {
  var val = type.value;
  dvd.style.display = "none";
  book.style.display = "none";
  furniture.style.display = "none";
  size.required = false;
  height.required = false;        
  width.required = false;
  furLength.required = false;
  weight.required = false;
  if (val == "dvd") {
  dvd.style.display = "block";
  size.required = true;
  } else if (val == "book") {
      book.style.display = "block";
      weight.required = true;
  } else if (val == "furniture") {
      furniture.style.display = "block";
      height.required = true;        
      width.required = true;
      length.required = true;
  }
}
  let price = document.getElementById("price");
  var regex = /^[1-9]+[0-9]*$/;
  function validateInput(){
    if(!regex.test(price.value)){
      price.setCustomValidity('Please, provide the data of indicated type');

    }
    else{
      price.setCustomValidity('');
    }
  }
  